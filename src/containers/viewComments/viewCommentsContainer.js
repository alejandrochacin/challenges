import { compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { getComments, changeCommentsError } from '../../redux/actions/comments';

import ViewComments from "./viewComments";

const mapStateToProps = (state) => ({
    errorComments: state.comments.error,
    isFetching: state.comments.fetching,
    comments: state.comments.data,
});

const mapDispatchToProps = {
    getComments,
    changeCommentsError,
  };

  export default compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps)
  )(ViewComments);
  