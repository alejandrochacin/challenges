import React, {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';

import CardComments from '../../components/cardComment';
import ModalError from '../../components/modalError';
import ModalForm from '../../components/modalForm';
import Spinner from "../../components/spinner";

const ViewComments = ({
  isFetching,
  errorComments,
  comments,
  getComments,
  changeCommentsError,
  match,
}) => {

  useEffect(() => {
    getComments(match.params.id)
  }, [getComments,match.params.id]);

  const [modal, setModal] = useState(false)

  const handleModal = () => {
    setModal(!modal);
  };

  const closeErrorModal = () => {
    if (errorComments !== false) changeCommentsError();
  };

  const addNewComment = (name, email, body, postId) => {
    comments.sort(function (a, b) {
      return a.id - b.id;
    });
    let newId = comments.slice(-1).pop().id + 1;
    let newComment = {
      id: newId,
      body: body,
      email: email,
      name: name,
      postId: postId,
    };
    comments.push(newComment);
  };

  if (isFetching) return <Spinner />;

  return (
		<div>
			{comments.length > 0 && (
				<div className="main-container-viewer">
					<h2>Comments</h2>
					{comments?.map((comment, index) => (
						<CardComments
							key={index}
							title={comment.name}
							body={comment.body}
							email={comment.email}
						/>
					))}
					<div className="button-container-comments">
						<Link to="/posts">
							<button className="button-new-comment-back">Go Posts</button>
						</Link>
						<button
							className="button-new-comment"
							onClick={handleModal}
						>
							Add New Comment
						</button>
						<ModalForm
							showModal={modal}
							handleModal={handleModal}
							addNewComment={addNewComment}
							postId={parseInt(match.params.id)}
						/>
					</div>
				</div>
			)}

			{comments.length === 0 && (
				<div>
					<h2>Empty Comments</h2>
					<Link to="/posts">
						<button className="button-new-comment-back">Go Posts</button>
					</Link>
				</div>
			)}
			{errorComments !== false && (
				<ModalError
					showModal={true}
					message={errorComments}
					closeErrorModal={closeErrorModal}
				/>
			)}
		</div>
	);
}

export default ViewComments;
