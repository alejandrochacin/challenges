import { connect } from 'react-redux';

import { getPosts, changePostError } from '../../redux/actions/posts';

import Viewer from './viewer';

const mapStateToProps = (state) => ({
  errorPosts: state.posts.error,
  isFetching: state.posts.fetching,
  posts: state.posts.data,
});

const mapDispatchToProps = {
  getPosts,
  changePostError,
};

export default connect(mapStateToProps, mapDispatchToProps)(Viewer);
