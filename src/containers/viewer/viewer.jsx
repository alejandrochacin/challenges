import React, { useEffect } from 'react';

import Card from '../../components/card';
import ModalError from '../../components/modalError';
import Spinner from '../../components/spinner';

import './style/viewer.css';

const Viewer = ({
  getPosts,
  changePostError,
  isFetching,
  posts,
  errorPosts,
}) => {
  useEffect(() => {
    getPosts();
  }, [getPosts]);

  const closeErrorModal = () => {
    if (posts?.errorPosts !== false) changePostError();
  };

  if (isFetching) return <Spinner />;

  return (
    <div>
      <div className='main-container-viewer' data-testid='containerView'>
        <h2>Posts</h2>
        {posts?.length > 0 ? (
          posts.map((post, index) => (
            <Card
              key={index}
              title={post.title}
              id={post.id}
              body={post.body}
            />
          ))
        ) : (
          //Show Empty with no Posts
          <div>
            <h1>Empty Posts</h1>
          </div>
        )}
        <div className='footer-container-posts'>
          <button
            data-testid='buttonreload'
            className='button-new-comment'
            onClick={getPosts}
          >
            Reload Posts
          </button>
        </div>
      </div>
      
      {errorPosts !== false && (
        <ModalError
          showModal={true}
          message={errorPosts}
          closeErrorModal={closeErrorModal}
        />
      )}
    </div>
  );
};

export default Viewer;
