import React from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import Viewer from '../containers/viewer';
import ViewComments from '../containers/viewComments';
import TitleBar from '../components/titlebar';

function Router() {
  return (
    <div>
      <BrowserRouter>
        <TitleBar />
        <Switch>
          <Route exact path='/'>
            <Redirect to='/posts' />
          </Route>
          <Route exact path='/posts' component={Viewer}></Route>
          <Route exact path='/comments/:id' component={ViewComments}></Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default Router;
