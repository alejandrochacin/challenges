import React from "react";
import Router from "./router/router";
import "./App.css";

function App() {
    return (
      <div>
        <Router/>
      </div>
    );
}

export default App;
