import React from "react";
import { shallow } from "enzyme";

import Card from "../card";

describe("CardComment", () => {
	let wrapper;
	const mockArgs = {
		key: 1,
		id: 1,
		title: "Title",
		body: "Body",
	};
    
  beforeEach( ()=> {	
    
    wrapper = shallow(
     <Card
       key={mockArgs.index}
       title={mockArgs.title}
       body={mockArgs.body}
       id={mockArgs.id}
     />
   );
  }
  );
  
	it("should be able to render a title", () => {
    const actual = wrapper.find('h3').text();
    expect(actual).toEqual('Post '+ mockArgs.id +' - '+mockArgs.title);
	});

	it("should be able to render a body", () => {
    const actual = wrapper.find('p').text();
    expect(actual).toEqual(mockArgs.body);
	});
	it("should be able to render a button", () => {
    const actual = wrapper.find('button').text();
    expect(actual).toEqual('View comments');
	});
});

