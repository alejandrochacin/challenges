import React from 'react';
import propTypes from 'prop-types';
import { Link } from 'react-router-dom';

import './style/card.css';

function Card({ id, title, body }) {
  const linkTo = {
    pathname: `/comments/${id}`,
  };

  return (
    <div className='card-container'>
      <div className='card-title-container'>
        <h3 className='title-card'>
          Post {id} - {title}
        </h3>
      </div>
      <p>{body}</p>
      <div className='button-card-container'>
        <Link to={linkTo}>
          <button className='button-card'>View comments</button>
        </Link>
      </div>
    </div>
  );
}

Card.propTypes ={
  id: propTypes.number,
  title: propTypes.string,
  body: propTypes.string,
}

export default Card;
