import React from "react";
import Modal from "react-modal";
import propTypes from "prop-types";

import "./style/modal.css";

if (process.env.NODE_ENV !== 'test') Modal.setAppElement("#root");

function ModalError({showModal,message,closeErrorModal}) {
	return (
		<div className="modal-container" id="modal">
			<Modal isOpen={showModal} contentLabel="Error Modal">
				<h2>Error:</h2>
				<p>{message}</p>
				<button className="button-new-comment-back" onClick={()=>{ closeErrorModal()}}> Close</button>
			</Modal>
		</div>
	);
	
}

ModalError.propTypes = {
    showModal: propTypes.bool,
	message:propTypes.string,
	closeErrorModal: propTypes.func,
  };
  
export default ModalError;
