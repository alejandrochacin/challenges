import React from 'react';
import { shallow } from 'enzyme';

import ModalError from '../modalError';

describe('ModalForm', () => {
  let wrapper;
  const mockArgs = {message:"Not Fetching"}
  const mockedEvent = { preventDefault: jest.fn() };
  //const errorComments = jest.fn();
  const closeErrorModal = jest.fn();

  beforeEach(() => {
    wrapper = shallow(
      <ModalError
        showModal={true}
		message={mockArgs.message}
		closeErrorModal={closeErrorModal}
      />
    );
    
  });

  it('should call closeErrorModal when clicking the Close Button', () => {
    wrapper.find('button').at(0).simulate('click', mockedEvent);
    expect(closeErrorModal).toHaveBeenCalledTimes(1);
  });

  it("should be able to render a error message", () => {
    const actual = wrapper.find('p').text();
    expect(actual).toEqual(mockArgs.message);
	});
});
