import React from 'react';
import { shallow } from 'enzyme';

import ModalForm from '../modalForm';

describe('ModalForm', () => {
  let wrapper;
  const postId = 1;
  const mockedEvent = { preventDefault: jest.fn() };
  const handleModal = jest.fn();
  const addNewComment = jest.fn();

  beforeEach(() => {
    wrapper = shallow(
      <ModalForm
        handleModal={handleModal}
        addNewComment={addNewComment}
        postId={postId}
      />
    );
  });

  it('should call handleModal when clicking the Close Button', () => {
    wrapper.find('button').at(0).simulate('click', mockedEvent);
    expect(handleModal).toHaveBeenCalledTimes(1);
  });

  it('should be able to change the input values and clicking the Save Comment button should call handleModal and addNewComment with the right parameters', () => {
    const mockedArgs = {
      email: 'email@email.com',
      name: 'name',
      comment: 'comment',
    };

    wrapper.find('input[name="email"]').simulate('change', {
      target: { name: 'email', value: mockedArgs.email },
    });
    wrapper.find('input[name="name"]').simulate('change', {
      target: { name: 'name', value: mockedArgs.name },
    });
    wrapper.find('textarea[name="comment"]').simulate('change', {
      target: { name: 'comment', value: mockedArgs.comment },
    });

    wrapper.find('button').at(1).simulate('click', mockedEvent);
    expect(handleModal).toHaveBeenCalledTimes(1);
    expect(addNewComment).toHaveBeenCalledTimes(1);
    expect(addNewComment).toHaveBeenCalledWith(
      mockedArgs.name,
      mockedArgs.email,
      mockedArgs.comment,
      postId
    );
  });
});
