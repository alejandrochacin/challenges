import { validateForm } from '../modalUtils';

global.window.alert = jest.fn();

describe('validateForm', () => {
  const mockedArg = {
    email: 'email@email.com',
    name: 'name',
    comment: 'comment',
  };

  it('should return false due to bad email format', () => {
    expect(validateForm({ ...mockedArg, email: '' })).toBe(false);
  });

  it('should return false due to empty name', () => {
    expect(validateForm({ ...mockedArg, name: '' })).toBe(false);
  });

  it('should return false due to empty comment', () => {
    expect(validateForm({ ...mockedArg, comment: '' })).toBe(false);
  });

  it('should return true', () => {
    expect(validateForm(mockedArg)).toBe(true);
  });
});
