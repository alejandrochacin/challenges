export const validateForm = ({ email, name, comment }) => {
  let pattern = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
  if (email.match(pattern) == null) {
    alert('Check Email Address');
    return false;
  }
  if (name.length === 0) {
    alert('Title is Empty');
    return false;
  }
  if (comment.length === 0) {
    alert('Comment is Empty');
    return false;
  }
  return true;
};
