import React, {useState} from "react";
import propTypes from "prop-types";
import Modal from "react-modal";

import { validateForm } from "./modalUtils";
import "./style/modal.css";

const ModalForm = ({showModal,handleModal, addNewComment, postId}) => {

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [comment, setComment] = useState("");
  
	const onClose = function (event) {
		event.preventDefault();
		handleModal();
	};

	const onValidate = function (event) {
		event.preventDefault();
		if (validateForm({ email, name, comment }) === true) {
			addNewComment(name, email, comment, postId);
			handleModal();
		}
	};

	return (
		<div className="modal-container" id="modal" data-testid="modalForm">
			<Modal isOpen={showModal} contentLabel="Minimal">
				<div className="modal-content">
					<h2>New Comment</h2>
					<div>
						<form>
							<div className="modal-input-content">
								<h3>Email: </h3>
								<input type="text" name="email" onChange={event => setEmail(event.target.value)} />
							</div>
							<div className="modal-input-content">
								<h3>Title: </h3>
								<input type="text" name="name" onChange={event => setName(event.target.value)} />
							</div>
							<div className="modal-textarea-content">
								<h3 className="modal-comment">Comment: </h3>
								<textarea name="comment" onChange={event => setComment(event.target.value)} />
							</div>
						</form>
					</div>
					<div className="button-modal-container">
						<button className="button-new-comment-back" onClick={onClose}>
							Close
						</button>
						<button className="button-card button-modal" onClick={onValidate}>
							Save Comment
						</button>
					</div>
				</div>
			</Modal>
		</div>
	);
};


ModalForm.propTypes = {
	showModal: propTypes.bool,
    handleModal: propTypes.func.isRequired,
    addNewComment: propTypes.func.isRequired,
    postId: propTypes.number,
  };

export default ModalForm;