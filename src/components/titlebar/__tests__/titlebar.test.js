import React from "react";
import { shallow } from "enzyme";

import TitleBar from "../titlebar";

describe("TitleBar", () => {
	let wrapper;

    const mockArgs = {
        title: "Social Network",
        name: "by Alejandro Chacin"
    }
  beforeEach( ()=> {	
    
    wrapper = shallow(
     <TitleBar/>
   );
  }
  );
	it("should be able to render a title on titlebar", () => {
    const actual = wrapper.find('h1').text();
    expect(actual).toEqual(mockArgs.title);
	});
	it("should be able to render a name on titlebar", () => {
    const actual = wrapper.find('h4').text();
    expect(actual).toEqual(mockArgs.name);
	});
});

