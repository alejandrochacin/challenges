import React from 'react'
import "./style/titlebar.css"

function Titlebar() {
    return (
        <div className="titlebar-container" data-testid="titlebar">
                <h1 className="title-tile-bar-container">Social Network</h1>
                <div className="title-bar-avatar">
                    <h4>by Alejandro Chacin</h4>
                </div>
            
        </div>
    )
}

export default Titlebar;