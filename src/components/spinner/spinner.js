import React from 'react';

function Spinner() {
  return (
    <div
      style={{
        position: 'fixed',
        left: '50%',
        top: '50%',
        transform: 'translate(-50%, -50%)',
      }}
    >
      Loading...
    </div>
  );
}

export default Spinner;
