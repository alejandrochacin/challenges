import React from "react";
import { shallow } from "enzyme";

import Spinner from "../spinner";

describe("Spinner", () => {
	let wrapper;

    const mockArgs = {
        message: "Loading..."
    }
  beforeEach( ()=> {	
    
    wrapper = shallow(
     <Spinner/>
   );
  }
  );
	it("should be able to render a loading", () => {
    const actual = wrapper.find('div').text();
    expect(actual).toEqual(mockArgs.message);
	});
});

