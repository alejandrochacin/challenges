import React from "react";
import propTypes from "prop-types";

import './style/commentsCard.css'

function CardComments ({id,title,body,email}) {
	return (
		<div className="card-container-comments" >
			<div className="title-card-container">
				<h3>Email:</h3>
				<p className="title-card-container-par">{email}</p>
			</div>
			<div className="title-card-container">
				<h3 >
					Title - 
				</h3>
				<p className="title-card-container-par">{title}</p>
			</div>
				<p>{body}</p>
		</div>
	);
}

CardComments.propTypes = {
    title: propTypes.string,
    body: propTypes.string,
	email: propTypes.string
  };

export default CardComments;

