import React from "react";
import { shallow } from "enzyme";

import CardComment from "../cardComment";

describe("CardComment", () => {
	let wrapper;
	const mockArgs = {
		key: 1,
		id: 1,
		title: "Title",
		body: "Body",
		email: "test@test.com",
	};

  beforeEach( ()=> {	
    
    wrapper = shallow(
     <CardComment
       key={mockArgs.index}
       title={mockArgs.title}
       body={mockArgs.body}
       email={mockArgs.email}
     />
   );
  }
  );
  
	it("should be able to render a email", () => {
    const actual = wrapper.find('p').at(0).text();
    expect(actual).toEqual(mockArgs.email);
	});
	it("should be able to render a title", () => {
    const actual = wrapper.find('.title-card-container-par').at(1).text();
    expect(actual).toEqual(mockArgs.title);
	});
	it("should be able to render a body", () => {
    const actual = wrapper.find('p').at(2).text();
    expect(actual).toEqual(mockArgs.body);
	});
});

