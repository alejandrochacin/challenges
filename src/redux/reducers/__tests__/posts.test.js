import {
  CHANGE_POSTS_ERROR,
  GET_POSTS_FAILED,
  GET_POSTS_REQUESTED,
  GET_POSTS_SUCCESS,
} from '../../actions/posts';
import postsReducer, { initialState } from '../posts';

describe('Posts Reducer', () => {
  it('GET_POSTS_REQUESTED', () => {
    const action = {
      type: GET_POSTS_REQUESTED,
    };

    const returnedState = postsReducer(undefined, action);

    const expectedReturnedState = {
      error: false,
      fetching: true,
      data: initialState.data,
    };
    expect(returnedState).toEqual(expectedReturnedState);
  });

  it('CHANGE_POSTS_ERROR', () => {
    const action = {
      type: CHANGE_POSTS_ERROR,
    };

    const returnedState = postsReducer(undefined, action);

    const expectedReturnedState = {
      error: false,
      fetching: true,
      data: initialState.data,
    };
    expect(returnedState).toEqual(expectedReturnedState);
  });

  it('GET_POSTS_SUCCESS', () => {
    const posts = 'posts';
    const action = {
      type: GET_POSTS_SUCCESS,
      posts,
    };

    const returnedState = postsReducer(undefined, action);

    const expectedReturnedState = {
      error: false,
      fetching: false,
      data: posts,
    };
    expect(returnedState).toEqual(expectedReturnedState);
  });

  it('GET_POSTS_FAILED', () => {
    const error = 'error';
    const action = {
      type: GET_POSTS_FAILED,
      error,
    };

    const returnedState = postsReducer(undefined, action);

    const expectedReturnedState = {
      error,
      fetching: false,
      data: initialState.data,
    };
    expect(returnedState).toEqual(expectedReturnedState);
  });

  it('initialState', () => {
    const action = {
      type: '',
    };

    const returnedState = postsReducer(undefined, action);

    expect(returnedState).toEqual(initialState);
  });
});
