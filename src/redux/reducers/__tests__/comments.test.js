import {
    CHANGE_COMMENTS_ERROR,
    GET_COMMENTS_FAILED,
    GET_COMMENTS_REQUESTED,
    GET_COMMENTS_SUCCESS,
  } from '../../actions/comments';
  import commentsReducer, { initialState } from '../comments';
  
  describe('Comments Reducer', () => {
    it('GET_COMMENTS_REQUESTED', () => {
      const action = {
        type: GET_COMMENTS_REQUESTED,
      };
  
      const returnedState = commentsReducer(undefined, action);
  
      const expectedReturnedState = {
        error: false,
        fetching: true,
        data: initialState.data,
      };
      expect(returnedState).toEqual(expectedReturnedState);
    });
  
    it('CHANGE_COMMENTS_ERROR', () => {
      const action = {
        type: CHANGE_COMMENTS_ERROR,
      };
  
      const returnedState = commentsReducer(undefined, action);
  
      const expectedReturnedState = {
        error: false,
        fetching: false,
        data: initialState.data,
      };
      expect(returnedState).toEqual(expectedReturnedState);
    });
  
    it('GET_COMMENTS_SUCCESS', () => {
      const comments = 'comments';
      const action = {
        type: GET_COMMENTS_SUCCESS,
        comments,
      };
  
      const returnedState = commentsReducer(undefined, action);
  
      const expectedReturnedState = {
        error: false,
        fetching: false,
        data: comments,
      };
      expect(returnedState).toEqual(expectedReturnedState);
    });
  
    it('GET_COMMENTS_FAILED', () => {
      const error = 'error';
      const action = {
        type: GET_COMMENTS_FAILED,
        error,
      };
  
      const returnedState = commentsReducer(undefined, action);
  
      const expectedReturnedState = {
        error,
        fetching: false,
        data: initialState.data,
      };
      expect(returnedState).toEqual(expectedReturnedState);
    });
  
    it('initialState', () => {
      const action = {
        type: '',
      };
  
      const returnedState = commentsReducer(undefined, action);
  
      expect(returnedState).toEqual(initialState);
    });
  });
  