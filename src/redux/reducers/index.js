import { combineReducers } from 'redux';
import posts from './posts';
import comments from './comments';

const rootReducer = combineReducers({
  posts: posts,
  comments: comments,
});

export default rootReducer;
