import {
  GET_POSTS_REQUESTED,
  GET_POSTS_PENDING,
  GET_POSTS_SUCCESS,
  GET_POSTS_FAILED,
  CHANGE_POSTS_ERROR,
} from '../actions/posts';

export const initialState = {
  error: false,
  fetching: true,
  data: [],
};

export default function posts(state = initialState, action) {
  switch (action.type) {
    case GET_POSTS_REQUESTED:
    case GET_POSTS_PENDING:
    return {
      ...state,
      fetching: true,
      error: false,
    };
    case CHANGE_POSTS_ERROR:
      return {
        ...state,
        error: false,
        fetching: true,
      };
    case GET_POSTS_SUCCESS:
      return {
        ...state,
        error: false,
        fetching: false,
        data: action.posts,
      };
    case GET_POSTS_FAILED:
      return {
        ...state,
        error: action.error,
        fetching: false,
        data: [],
      };

    default:
      return state;
  }
}
