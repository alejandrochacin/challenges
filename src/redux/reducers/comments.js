import {
  GET_COMMENTS_REQUESTED,
  GET_COMMENTS_PENDING,
  GET_COMMENTS_SUCCESS,
  GET_COMMENTS_FAILED,
  CHANGE_COMMENTS_ERROR,
} from '../actions/comments';

export const initialState = {
  data: [],
  fetching: true,
  error: false,
};

export default function comments(state = initialState, action) {
  switch (action.type) {
    case GET_COMMENTS_REQUESTED:
    case GET_COMMENTS_PENDING:
      return {
        ...state,
        fetching: true,
        error: false,
      };
    case CHANGE_COMMENTS_ERROR:
      return {
        ...state,
        fetching: false,
        error: false,
      };
    case GET_COMMENTS_SUCCESS:
      return {
        ...state,
        fetching: false,
        error: false,
        data: action.comments,
      };
    case GET_COMMENTS_FAILED:
      return {
        ...state,
        data: [],
        fetching: false,
        error: action.error,
      };

    default:
      return state;
  }
}
