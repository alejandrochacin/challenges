import {
  getComments,
  getCommentsPending,
  getCommentsSuccess,
  getCommentsFailed,
  changeCommentsError,
  GET_COMMENTS_REQUESTED,
  GET_COMMENTS_FAILED,
  GET_COMMENTS_SUCCESS,
  GET_COMMENTS_PENDING,
  CHANGE_COMMENTS_ERROR,
} from '../comments';

describe('Comments Actions', () => {
  it('getComments', () => {
    const expectedAction = {
      type: GET_COMMENTS_REQUESTED,
    };
    const action = getComments();
    expect(action).toEqual(expectedAction);
  });

  it('getCommentsPending', () => {
    const expectedAction = {
      type: GET_COMMENTS_PENDING,
    };
    const action = getCommentsPending();
    expect(action).toEqual(expectedAction);
  });

  it('getCommentsSuccess', () => {
    const comments = 'comments';
    const expectedAction = {
      type: GET_COMMENTS_SUCCESS,
      comments: comments,
    };
    const action = getCommentsSuccess(comments);
    expect(action).toEqual(expectedAction);
  });

  it('getCommentsFailed', () => {
    const error = 'error';
    const expectedAction = {
      type: GET_COMMENTS_FAILED,
      error,
    };
    const action = getCommentsFailed(error);
    expect(action).toEqual(expectedAction);
  });

  it('changeCommentsError', () => {
    const expectedAction = {
      type: CHANGE_COMMENTS_ERROR,
    };
    const action = changeCommentsError();
    expect(action).toEqual(expectedAction);
  });
});
