import {
  getPosts,
  getPostsPending,
  getPostsSuccess,
  getPostsFailed,
  changePostError,
  GET_POSTS_REQUESTED,
  GET_POSTS_FAILED,
  GET_POSTS_SUCCESS,
  GET_POSTS_PENDING,
  CHANGE_POSTS_ERROR,
} from '../posts';

describe('Posts Actions', () => {
  it('getPosts', () => {
    const expectedAction = {
      type: GET_POSTS_REQUESTED,
    };
    const action = getPosts();
    expect(action).toEqual(expectedAction);
  });

  it('getPostsPending', () => {
    const expectedAction = {
      type: GET_POSTS_PENDING,
    };
    const action = getPostsPending();
    expect(action).toEqual(expectedAction);
  });

  it('getPostsSuccess', () => {
    const posts = 'posts';
    const expectedAction = {
      type: GET_POSTS_SUCCESS,
      posts: posts,
    };
    const action = getPostsSuccess(posts);
    expect(action).toEqual(expectedAction);
  });

  it('getPostsFailed', () => {
    const error = 'error';
    const expectedAction = {
      type: GET_POSTS_FAILED,
      error,
    };
    const action = getPostsFailed(error);
    expect(action).toEqual(expectedAction);
  });

  it('changePostError', () => {
    const expectedAction = {
      type: CHANGE_POSTS_ERROR,
    };
    const action = changePostError();
    expect(action).toEqual(expectedAction);
  });
});
