export const GET_POSTS_REQUESTED = 'GET_POSTS_REQUESTED';
export const getPosts = () => ({
  type: GET_POSTS_REQUESTED,
});

export const GET_POSTS_PENDING = 'GET_POSTS_PENDING';
export const getPostsPending = () => ({
  type: GET_POSTS_PENDING,
});

export const GET_POSTS_SUCCESS = 'GET_POSTS_SUCCESS';
export const getPostsSuccess = (posts) => ({
  type: GET_POSTS_SUCCESS,
  posts: posts,
});

export const GET_POSTS_FAILED = 'GET_POSTS_FAILED';
export const getPostsFailed = (error) => ({
  type: GET_POSTS_FAILED,
  error,
});

export const CHANGE_POSTS_ERROR = 'CHANGE_POSTS_ERROR';
export const changePostError = () => ({
  type: CHANGE_POSTS_ERROR,
});
