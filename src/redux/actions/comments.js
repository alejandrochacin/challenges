export const GET_COMMENTS_REQUESTED = 'GET_COMMENTS_REQUESTED';
export const getComments = (originPost) => ({
  type: GET_COMMENTS_REQUESTED,
  payload: originPost,
});

export const GET_COMMENTS_PENDING = 'GET_COMMENTS_PENDING';
export const getCommentsPending = () => ({
  type: GET_COMMENTS_PENDING,
});

export const GET_COMMENTS_SUCCESS = 'GET_COMMENTS_SUCCESS';
export const getCommentsSuccess = (comments) => ({
  type: GET_COMMENTS_SUCCESS,
  comments: comments,
});

export const GET_COMMENTS_FAILED = 'GET_COMMENTS_FAILED';
export const getCommentsFailed = (error) => ({
  type: GET_COMMENTS_FAILED,
  error,
});

export const CHANGE_COMMENTS_ERROR = 'CHANGE_COMMENTS_ERROR';
export const changeCommentsError = () => ({
  type: CHANGE_COMMENTS_ERROR,
});
