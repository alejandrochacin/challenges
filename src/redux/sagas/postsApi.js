export function getApi() {
  
  const apiURL = 'https://jsonplaceholder.typicode.com/posts';
  return fetch(apiURL, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
  })
    .then((response) => response.json())
    .catch((error) => {
      throw error;
    });
}
