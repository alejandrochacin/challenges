import { call, put, takeEvery } from 'redux-saga/effects';

import {
  getCommentsFailed,
  getCommentsPending,
  getCommentsSuccess,
  GET_COMMENTS_REQUESTED,
} from '../../actions/comments';
import { getApi } from '../commentsApi';

import commentsSaga, { fetchComments } from '../comments';

jest.mock('../commentsApi');

describe('Comments Saga', () => {
  describe('fetchComments', () => {
    
    it('Positive', () => {
      const payload = 1;
      const gen = fetchComments({payload:1});

      const step1 = gen.next();
      expect(step1.value).toEqual(put(getCommentsPending()));

      const step2 = gen.next();
      expect(step2.value).toEqual(call(getApi,payload));

      const step2return = 'Comments';
      const step3 = gen.next(step2return);
      expect(step3.value).toEqual(put(getCommentsSuccess(step2return)));

      const finalStep = gen.next();
      expect(finalStep.value).toEqual(undefined);
      expect(finalStep.done).toEqual(true);
    });

    it('Negative', () => {
      const gen = fetchComments({payload:1});

      gen.next();
      const error = { message: 'errorMessage' };
      const step1 = gen.throw(error);
      expect(step1.value).toEqual(put(getCommentsFailed(error.message)));

      const finalStep = gen.next();
      expect(finalStep.value).toEqual(undefined);
      expect(finalStep.done).toEqual(true);
    });
  });

  describe('CommentsSaga', () => {
    it('Positive', () => {
      const gen = commentsSaga();

      const step1 = gen.next();
      expect(step1.value).toEqual(takeEvery(GET_COMMENTS_REQUESTED, fetchComments));

      const finalStep = gen.next();
      expect(finalStep.value).toEqual(undefined);
      expect(finalStep.done).toEqual(true);
    });
  });
});
