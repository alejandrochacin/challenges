import { call, put, takeEvery } from 'redux-saga/effects';

import {
  getPostsFailed,
  getPostsPending,
  getPostsSuccess,
  GET_POSTS_REQUESTED,
} from '../../actions/posts';
import { getApi } from '../postsApi';

import postsSaga, { fetchPosts } from '../posts';

jest.mock('../postsApi');

describe('Posts Saga', () => {
  describe('fetchPosts', () => {
    
    it('Positive', () => {
      const gen = fetchPosts();

      const step1 = gen.next();
      expect(step1.value).toEqual(put(getPostsPending()));

      const step2 = gen.next();
      expect(step2.value).toEqual(call(getApi));

      const step2return = 'posts';
      const step3 = gen.next(step2return);
      expect(step3.value).toEqual(put(getPostsSuccess(step2return)));

      const finalStep = gen.next();
      expect(finalStep.value).toEqual(undefined);
      expect(finalStep.done).toEqual(true);
    });

    it('Negative', () => {
      const gen = fetchPosts();

      gen.next();
      const error = { message: 'errorMessage' };
      const step1 = gen.throw(error);
      expect(step1.value).toEqual(put(getPostsFailed(error.message)));

      const finalStep = gen.next();
      expect(finalStep.value).toEqual(undefined);
      expect(finalStep.done).toEqual(true);
    });
  });

  describe('postsSaga', () => {
    it('Positive', () => {
      const gen = postsSaga();

      const step1 = gen.next();
      expect(step1.value).toEqual(takeEvery(GET_POSTS_REQUESTED, fetchPosts));

      const finalStep = gen.next();
      expect(finalStep.value).toEqual(undefined);
      expect(finalStep.done).toEqual(true);
    });
  });
});
