import { call, put, takeEvery } from 'redux-saga/effects';

import {
  getPostsFailed,
  getPostsPending,
  getPostsSuccess,
  GET_POSTS_REQUESTED,
} from '../actions/posts';
import { getApi } from './postsApi';

export function* fetchPosts() {
  try {
    yield put(getPostsPending());
    const posts = yield call(getApi);
    yield put(getPostsSuccess(posts));
  } catch (e) {
    yield put(getPostsFailed(e.message));
  }
}

function* postsSaga() {
  yield takeEvery(GET_POSTS_REQUESTED, fetchPosts);
}

export default postsSaga;
