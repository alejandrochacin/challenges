import { all } from "redux-saga/effects";

import commentsSaga from "./comments";
import postsSaga from "./posts";

export function* postsSagaSvc() {
    yield all([postsSaga()]);
};

export  function* commentsSagaSvc() {
    yield all([commentsSaga()]);
};