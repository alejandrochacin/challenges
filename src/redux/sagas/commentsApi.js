export function getApi(originPost) {
    
    const apiURL = `https://jsonplaceholder.typicode.com/posts/${originPost}/comments`;
    return fetch(apiURL, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((response) => response.json())
      .catch((error) => {
        throw error;
      });
  }
  