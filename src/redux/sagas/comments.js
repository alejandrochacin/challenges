import { call, put, takeEvery } from "redux-saga/effects";

import {
	getCommentsFailed,
	getCommentsPending,
	getCommentsSuccess,
	GET_COMMENTS_REQUESTED,
} from "../actions/comments";
import { getApi } from "./commentsApi";

export function* fetchComments({ payload }) {
	try {
		yield put(getCommentsPending());
		const comments = yield call(getApi, payload);
		yield put(getCommentsSuccess(comments));
	} catch (e) {
		yield put(getCommentsFailed(e.message));
	}
}

function* commentsSaga() {
	yield takeEvery(GET_COMMENTS_REQUESTED, fetchComments);
}

export default commentsSaga;
