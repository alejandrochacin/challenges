import { createStore, compose,applyMiddleware } from 'redux';
import rootReducer from './reducers/index';
import createSagaMiddleware from "redux-saga";
import {postsSagaSvc, commentsSagaSvc} from './sagas/index'

const sagaMiddleware = createSagaMiddleware();

const store = compose(
  applyMiddleware(sagaMiddleware),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  
)(createStore)(rootReducer);

sagaMiddleware.run(postsSagaSvc);

sagaMiddleware.run(commentsSagaSvc);

export default store;